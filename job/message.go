package job

import (
	"fmt"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/job"
	"go.mongodb.org/mongo-driver/mongo"
	"reflect"
	"time"
)

var JobMessage *job.Executor

func BroadCastMessage(jobItem *job.JobItem) error {
	fmt.Println("1234")
	fmt.Println(reflect.TypeOf(jobItem))
	return nil
}

func InitMessage(session *mongo.Database) {
	temp1 := make(map[string]string)
	temp1["name"] = "long"
	temp1["gender"] = "male"
	temp2 := &job.JobItemMetadata{
		UniqueKey: "",
		SortedKey: "",
		Keys:      []string{},
		Topic:     "default",
		ReadyTime: new(time.Time),
	}
	JobMessage = &job.Executor{
		ColName: "message",
	}
	JobMessage.Init(session, session.Name())
	JobMessage.Push(temp1, temp2)
	JobMessage.SetTopicConsumer("default", BroadCastMessage)
	JobMessage.StartConsume()
}
