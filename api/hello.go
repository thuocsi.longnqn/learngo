package api

import (
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/common"
)

func Hello(req sdk.APIRequest, res sdk.APIResponder) error {
	return res.Respond(&common.APIResponse{
		Status:  common.APIStatus.Ok,
		Message: "Hello world",
	})
}
