package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/client"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/common"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/db"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/schedule"
	"go-sdk-api/api"
	"go-sdk-api/job"
	"go-sdk-api/model"
	"go.mongodb.org/mongo-driver/mongo"
	"reflect"
	"time"
)

var app *sdk.App
var newClient *client.APIClient

var configDB schedule.ConfigDB

func connectDB() {
	dbDev := app.SetupDBClient(db.Configuration{
		Address:  "mongodb://127.0.0.1:27017",
		Username: "nqnlong",
		Password: "123456",
		DBName:   "GolangAPI",
	}, func(database *mongo.Database) error {
		model.InitUserDB(database)
		configDB.InitAndStart(database)
		fmt.Println("da chay qua")
		return nil
	})
	dbDev.Connect()
}

func test(req sdk.APIRequest, res sdk.APIResponder) error {
	acc := client.APIClientConfiguration{
		Address:  "api.zippopotam.us/AD",
		Protocol: "HTTP",
	}

	var ac client.APIClient
	ac = client.NewAPIClient(&acc)
	fmt.Println(reflect.TypeOf(ac))
	//rc := ac.(client.RestClient)
	var rc *client.RestClient
	rc = ac.(*client.RestClient)
	fmt.Println(reflect.TypeOf(rc))

	result, err := rc.MakeHTTPRequestWithKey(client.HTTPMethods.Get, nil, nil, nil, req.GetPath(), nil)
	fmt.Println(err)
	var rs interface{}
	json.Unmarshal(result.Content, &rs)
	fmt.Println(reflect.TypeOf(rs))

	//fmt.Println((*ac))

	return res.Respond(&common.APIResponse{
		Status: common.APIStatus.Ok,
		Data:   &rs,
	})
}

func testWorker(req sdk.APIRequest, res sdk.APIResponder) error {
	appworker := sdk.AppWorker{
		Task: func() {
			err := api.ReadAllUsers(req, res)
			if err != nil {
				return
			}
		},
	}
	appworker.SetDelay(3)
	appworker.SetRepeatPeriod(1)
	appworker.Execute()
	return res.Respond(&common.APIResponse{
		Status:  common.APIStatus.Ok,
		Message: "nice",
	})
}

func newProcess(t *time.Time, c *schedule.Config) (error, string, *time.Time) {
	fmt.Println("tocpic:", c.Topic)
	return nil, c.Topic, t
}

var process = make(map[string]schedule.Process)

func testjob(req sdk.APIRequest, res sdk.APIResponder) error {
	job.JobMessage.StartConsume()
	return res.Respond(&common.APIResponse{
		Status:  common.APIStatus.Ok,
		Message: "nice",
	})
}

func main() {
	app = sdk.NewApp("my intern project")

	process["new"] = newProcess

	configDB = *schedule.NewConfigDB("schedule", process)
	connectDB()

	app.SetupDBClient(db.Configuration{
		Address:  "mongodb://127.0.0.1:27017",
		Username: "nqnlong",
		Password: "123456",
		DBName:   "GolangAPI",
		//AuthDB:      config.Config.JobAuthDB,
		DoWriteTest: true,
	}, onDBJobConnected)

	server, _ := app.SetupAPIServer("HTTP")

	server.SetHandler(common.APIMethod.GET, "/hello", api.Hello)

	server.SetHandler(common.APIMethod.GET, "/user/:id", api.ReadById)
	server.SetHandler(common.APIMethod.GET, "/user", api.ReadUser)
	server.SetHandler(common.APIMethod.GET, "/users", api.ReadAllUsers)

	server.SetHandler(common.APIMethod.POST, "/create", api.CreateUser)
	server.SetHandler(common.APIMethod.POST, "/createall", api.CreateAllUsers)

	server.SetHandler(common.APIMethod.PUT, "/put/:id", api.UpdateUser)
	server.SetHandler(common.APIMethod.PUT, "/putall", api.UpdateAllUsers)

	server.SetHandler(common.APIMethod.DELETE, "/delete/:id", api.DeleteUser)
	server.SetHandler(common.APIMethod.DELETE, "/deleteall", api.DeleteAllUsers)

	server.SetHandler(common.APIMethod.GET, "/AD100", test)

	server.SetHandler(common.APIMethod.GET, "/worker", testWorker)

	server.SetHandler(common.APIMethod.GET, "/job", testjob)

	server.Expose(1323)

	app.Launch()
}

func onDBJobConnected(database *mongo.Database) error {
	job.InitMessage(database)
	return nil
}
