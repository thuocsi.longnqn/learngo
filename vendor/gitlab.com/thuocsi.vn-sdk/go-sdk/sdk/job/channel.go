package job

import (
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/common"
	"gitlab.com/thuocsi.vn-sdk/go-sdk/sdk/db"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"sync"
	"time"
)

// ExecutionChannel ...
type ExecutionChannel struct {
	name        string
	item        chan *JobItem
	isActive    bool
	processing  bool
	consumerMap map[string]ExecutionFn
	jobDB       *db.Instance
	consumedDB  *db.Instance
	lock        *sync.Mutex
	config      *ExecutorConfiguration
}

func (c *ExecutionChannel) start() {

	for true {

		// wait from the channel
		item := <-c.item

		// the more the item fail, it require wait more time
		// limit max = <size of log> second
		if item.LastFail != nil && time.Since(*item.LastFail).Seconds() < float64(c.config.MaximumWaitToRetryS) {
			if item.Log != nil {
				if len(*item.Log) < 3 {
					time.Sleep(time.Duration(200*len(*item.Log)) * time.Millisecond)
				} else {
					time.Sleep(time.Duration(len(*item.Log)) * time.Second)
				}
			} else {
				time.Sleep(1 * time.Second)
			}
		}

		// check if there are exist any other item with same sorted key
		validOrder := c.validateItemOrder(item)
		var err error

		if validOrder {
			start := time.Now()
			consumer := c.consumerMap[item.Topic]
			if c.config.ParallelTopicProcessing {
				consumer = c.consumerMap["default"]
			}
			err = (error)(&common.Error{
				Type:    "INIT_MISSING",
				Message: "Consumer for " + item.Topic + " is not ready.",
			})

			if consumer != nil {
				err = consumer(item)
			}

			if err == nil { // if successfully
				c.jobDB.Delete(&JobItem{
					ID: item.ID,
				})
				t := time.Since(start).Nanoseconds() / 1000000
				item.ProcessTimeMS = int(t)
				item.ID = nil
				c.consumedDB.Create(item)
			}
		}

		if !validOrder || err != nil { // if consuming function return errors
			if err == nil {
				err = (error)(&common.Error{
					Type:    "WRONG_ORDER",
					Message: "This item is consumed at wrong order.",
				})
			}

			errStr := c.name + " " + time.Now().Format("2006-01-02T15:04:05+0700") + " " + err.Error()
			l := 0
			if item.Log == nil {
				item.Log = &[]string{errStr}
			} else {
				l = len(*item.Log)
				if l > c.config.LogSize { // crop if too long
					tmp := (*item.Log)[l-c.config.LogSize+1:]
					item.Log = &tmp
				}
				log := append(*item.Log, errStr)
				item.Log = &log
			}

			if validOrder {
				item.FailCount += 1
			}

			// re-push to end of queue for too-many-fail item
			if l > c.config.FailThreshold {
				c.repushItem(item)
			} else {
				// update item
				now := time.Now()
				c.jobDB.UpdateOne(&JobItem{
					ID: item.ID,
				}, &JobItem{
					Log:       item.Log,
					ProcessBy: "NONE",
					LastFail:  &now,
					FailCount: item.FailCount,
				}, &options.FindOneAndUpdateOptions{
					Sort: bson.M{"sort_index": 1},
				})
			}
		}
		c.processing = false
	}

}

func (c *ExecutionChannel) repushItem(item *JobItem) {
	repushedItem := &JobItem{
		Data:        item.Data,
		Keys:        item.Keys,
		ProcessBy:   "NONE",
		SortedKey:   item.SortedKey,
		SortIndex:   item.SortIndex,
		Topic:       item.Topic,
		RepushCount: item.RepushCount + 1,
		Log:         item.Log,
		FailCount:   item.FailCount,
		ReadyTime:   item.ReadyTime,
	}
	c.jobDB.Create(repushedItem)
	c.jobDB.Delete(&JobItem{
		ID: item.ID,
	})
}

func (c *ExecutionChannel) putItem(item *JobItem) bool {
	c.item <- item
	return true
}

func (c *ExecutionChannel) validateItemOrder(item *JobItem) bool {
	if item.SortedKey == "" {
		return true
	}

	itemRs := c.jobDB.Query(&JobItem{
		SortedKey: item.SortedKey,
	}, 0, 1, &bson.M{"sort_index": 1})

	if itemRs.Status == common.APIStatus.Ok {
		firstItem := itemRs.Data.([]*JobItem)[0]
		if firstItem.SortIndex < item.SortIndex {
			return false
		}
	}
	return true
}
